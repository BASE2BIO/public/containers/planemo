FROM debian:stable-slim

MAINTAINER Jeremy Volkening <jeremy.volkening@base2bio.com>

COPY test_tool /test_tool

ENV HOME_PREFIX /home/planemo
ENV PLANEMO_PREFIX ${HOME_PREFIX}/.planemo
ENV VIRTUAL_ENV ${PLANEMO_PREFIX}/planemo
ENV PLANEMO_CONDA_PREFIX ${PLANEMO_PREFIX}/conda
ENV PLANEMO_GALAXY_ROOT ${PLANEMO_PREFIX}/galaxy
ARG GALAXY_BRANCH=master
ENV GALAXY_BRANCH ${GALAXY_BRANCH}

RUN apt-get update && apt-get install -y \
      build-essential \
      curl \
      git \
      libbz2-dev \
      liblzma-dev \
      libssl-dev \
      python3 \
      python3-venv \
      python3-pip \
      rustc \
   # install planemo via pip (conda too slow solving environment)
   && useradd -m -U planemo \
   && python3 -m venv $VIRTUAL_ENV \
   && . $VIRTUAL_ENV/bin/activate \
   && pip3 install planemo \
   && pip3 install Pygments \
   # initialize conda to save time later, although makes larger image
   && $VIRTUAL_ENV/bin/planemo conda_init \
   && cd ${PLANEMO_PREFIX} \
   # install Galaxy to save time later, although makes larger image
   && git clone --depth 1 --single-branch --branch ${GALAXY_BRANCH} https://github.com/galaxyproject/galaxy.git \
   && rm -rf galaxy/\.git \
   && chown -R planemo: ${HOME_PREFIX} \
   # by testing a tool here, we finish initializing Galaxy, saving time later
   && su - planemo -c "$VIRTUAL_ENV/bin/planemo test --galaxy_root ${PLANEMO_GALAXY_ROOT} --conda_prefix ${PLANEMO_CONDA_PREFIX} /test_tool/test_tool.xml" \
   # cleanup unneeded build files to save disk space
   && rm ${HOME_PREFIX}/tool_test_output* \
   && rm -rf /test_tool \
   && apt-get purge -y \
        build-essential \
        libbz2-dev \
        liblzma-dev \
        libssl-dev \
        rustc \
   && apt-get autoremove --purge -y \
   && apt-get clean \
   && rm -rf $PLANEMO_CONDA_PREFIX/pkgs/cache \
   && rm -rf ${HOME_PREFIX}/.cache/pip \
   && rm -rf ${PLANEMO_PREFIX}/gx_venv_3/src/* \
   # finish setting up environment
   && chown -R planemo: ${HOME_PREFIX}

ENV PATH "$VIRTUAL_ENV/bin:$PATH"

USER planemo
WORKDIR ${HOME_PREFIX}

ENTRYPOINT ["planemo"]
