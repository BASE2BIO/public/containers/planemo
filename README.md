This container image provides an executable environment for
[Planemo](https://github.com/galaxyproject/planemo), a suite of command-line
utilities to assist in developing Galaxy and Common Workflow Language
artifacts.

To use it, you need to mount your tools directory, typically read-only. In
addition, it helps to use a persistent volume for your planemo configuration
directory -- this avoids the need for planemo to re-clone and re-initialize
the local Galaxy instance each time you run the tool, which is a rather
lengthy process. This is done below using the `-v planemo:/root/.planemo`
parameter. The recommended usage is:

```bash
# for example, test the tool under 'tools/sometool/sometool.xml'
docker run -v <path_to_tools>:/tools:ro -v planemo:/root/.planemo -it base2bio/planemo test /tools/sometool/sometool.xml
# etc
```

**NOTE:** this container runs as the root user, because it was problematic to
mount read/write volumes when run as a regular user. It is recommended to
configure your docker server to use
[user namespaces](https://docs.docker.com/engine/security/userns-remap/) to
alleviate the security concerns associated with running containers as root.
